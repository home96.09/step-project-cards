class Select {
  constructor(classCss, name, stringValues) {
    this.classSelect = classCss;
    this.name = name;
    this.stringValues = stringValues;
  }
  render() {
    const arrayString = this.stringValues.split(",");
    const [changeDoctor, cardiologist, dentist, therapist, doctor] =
      arrayString;
    return `<select   class="${doctor} ${this.classSelect}" name=${this.name} >
            <option selected >${changeDoctor}</option>
            <option value="${cardiologist}">${cardiologist}</option>
            <option value="${dentist}">${dentist}</option>
            <option value="${therapist}">${therapist}</option>
        </select> 
`;
  }
}

export { Select };
