import API from "../../function/api/API";
import { visitLayout } from "../../function/filters/filters";
import { arrayVisitsView } from "../../scripts";
import { VisitCardiologist, VisitDentist, VisitTherapist } from "./visitsModel";

function getVisits() {
  API.getCards().then((responses) => {
    responses.forEach((response) => {
      createVisitForView(response);
    });
    visitLayout(arrayVisitsView);
  });
}

function createVisitForView(object) {
  //создает обьект согласно нужного класса. Добавялет обьект в массив для отображения

  let cardObjView = {};

  if (object.doctor === "Стоматолог") {
    cardObjView = new VisitDentist(object);
  } else if (object.doctor === "Кардиолог") {
    cardObjView = new VisitCardiologist(object);
  } else if (object.doctor === "Терапевт") {
    cardObjView = new VisitTherapist(object);
  }

  arrayVisitsView.push(cardObjView);
}

export { getVisits };
export { createVisitForView };
